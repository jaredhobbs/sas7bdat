data mixedvalues_empty(compress=char);
input integer float text $ time :TIME. date :yymmdd10.;
format time :TIME. date yymmdd10.;
datalines;
1 0.1 abc 00:00 1980-01-1
2 0.2 def 02:22 1990-12-31
3 0.3 GHI 23:59 2004-02-29
4 0.4 . 00:00 2000-01-01
5 0.5 MNO 05:55 2111-11-11
6  PQR 05:55 2111-11-11
;

